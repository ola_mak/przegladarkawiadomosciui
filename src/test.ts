import '../scss/main.scss';
import '../images/logo.png';

declare global {
    interface Window {
        openNav: any;
        closeNav: any;
        getCategory: any;
    }
}

window.openNav = function() {
    document.getElementById("myMenu")!.classList.add("menuOpened");
    document.getElementById("myMenu")!.classList.remove("menuClosed");
    document.getElementById("h1")!.classList.remove("mobile");
    document.getElementById("h1")!.classList.add("mobile2");

 }

window.closeNav = function() {
    document.getElementById("myMenu")!.classList.add("menuClosed");
    document.getElementById("myMenu")!.classList.remove("menuOpened");
    document.getElementById("h1")!.classList.add("mobile");
    document.getElementById("h1")!.classList.remove("mobile2");
}

window.getCategory = function(country: string) {
    let news: HTMLElement = document.getElementById("news")!;

    // while (news!.hasChildNodes()) {
    //     news!.removeChild(news.firstChild!);
    // }
    loadNews("http://localhost:8080/news/" + country +"/technology");
}


function populateData(articles: any) {
    let newsContainer: HTMLElement = document.getElementById("news")!;
    var news = document.createElement("div");
    news.setAttribute("class", "columns");
    newsContainer.appendChild(news);


    for(var i=0; articles.length; i++) {
        var div = document.createElement("div");
        div.setAttribute('class', "column");
        news.appendChild(div);

        var header = document.createElement("h2");
        header.innerText = articles[i].title;
        div.appendChild(header);

        var column = document.createElement("P");
        column.innerText = articles[i].description;
        div.appendChild(column);


        newsContainer.replaceChild(news, newsContainer.children[0]);

    }

}

function loadNews(url: string) {
    let xmlHttpRequest = new XMLHttpRequest();
    xmlHttpRequest.open("GET", url, true);
    xmlHttpRequest.onreadystatechange = function() {
        if (this.readyState === 4 && this.status === 200) {
            const news = JSON.parse(this.responseText);
            console.log(news);
            populateData(news.articles);
        }
    }
    xmlHttpRequest.send();
}

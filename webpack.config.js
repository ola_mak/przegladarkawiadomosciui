var HtmlWebpackPlugin = require('html-webpack-plugin');

module.exports = {
    entry: './src/test.ts',
    output: {
        path: __dirname + '/dist',
        filename: 'src/test.js'
    },
    plugins: [
        new HtmlWebpackPlugin({
            hash: true,

            template: './test.html',
            filename: './test.html' //relative to root of the application
        })
    ],
    module: {
        rules: [
            {
                test: /\.tsx?$/,
                use: {
                    loader: 'ts-loader',
                },
            },
            {
                test: /\.s[ac]ss$/i,
                use: [
                    // Creates `style` nodes from JS strings
                    'style-loader',
                    // Translates CSS into CommonJS
                    'css-loader',
                    // Compiles Sass to CSS
                    'sass-loader',
                ],
            },
            {
                test: /\.(png|jpg|gif)$/,
                use: [{
                    // 'url-loader?limit=10000',
                    // 'img-loader',
                    loader: 'file-loader',
                    options: {
                        name: 'images/[name].[ext]'
                    }
                }]
            },

        ]
    },
};
